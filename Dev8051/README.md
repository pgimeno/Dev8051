# Dev8051 Software Development Board for 8051

## Introduction

This project introduces a pretty minimalistic, yet quite versatile, software development board for 8051-type microcontrollers. It requires just four ICs: the micro itself, the address latch, a RAM and a quad NAND-gate chip.

For communications it uses the regular serial port of the 8051. There's no built-in RS-232 level driver, because TTL USB (e.g. CH340C based) serial port dongles are common and cheap, and there's no need to do voltage conversion back and forth this way. Also, it's possible to build an add-on board for RS-232 level conversion, if that's really necessary.

One requisite is that the micro must have an internal programable ROM, be it OTP ROM, EPROM, EEPROM, Flash or anything. I also wrote an accompanying software package, [deb51](../deb51), which takes slightly over 2KB, so a micro with at least 4KB is advised. Otherwise you can grab deb51, trim some features from cmds.asm and fit it into 2KB. The board and the software are also designed to work with external RAM only; if the MCU has internal RAM that is accessed via MOVX, you have to disable it if possible by modifying the deb51 initialization, or use a different micro without that RAM.

As is, the board is designed to work with a 128KB RAM (I used a TC551001CP in my case, but others should work - check the datasheet). It shouldn't be too difficult to modify it to work with a smaller RAM; see [Internals](#Internals) below for details.

## Usage/Features

The board has a switch to allow you to select one of two modes. In "Program" mode, only 64KB of RAM are visible. These 64KB are configured as both external data and program memory simultaneously ("Von Neumann" architecture), except that in this mode, the internal ROM of the micro is active, and therefore not all 64KB are visible as program memory, only as data memory.

In "Run" mode, however, the micro's ROM is disabled, and what was shared program/data memory is now *program* memory only. The other 64KB of the RAM become now visible, as *data* memory ("Modified Harvard" architecture).

When the MCU's internal ROM contains a program that allows loading data from a file to external memory, like deb51, this setup allows you to run software designed for separate program and data memory, of any size. One such piece of software is BASIC-52.

So, as an example, in order to run BASIC-52 you would go like this:

- Initially you would have deb51 loaded into the internal ROM.
- Switch to Program mode, reset, and use the `L` command to load an Intel HEX file.
- Upload the file in ASCII mode using your communications program.
- Switch to Run mode and reset. Now BASIC-52 is ready to run; all you need to do is press the space bar to see the banner.

Unfortunately, due to the minimalistic design of the board, the mode selector does not automatically reset when changing modes.

If you need to preload the data memory in Run mode, you can proceed as follows:

- Switch to Program mode and reset, and proceed as before to load deb51 itself into RAM.
- Switch to Run mode again and reset. Now you're running deb51 in run mode.
- Use the `L` command again to load the data you wish to preload into data RAM.
- Switch back to Program mode, and load the program you want to run.
- Keep reset pressed when changing the mode back. This will prevent that when switching, the program counter is in a random position, which could with some bad luck execute an unwanted MOVX instruction that overwrites the data RAM.

There's a GPIO header in the board. If you have something connected that should not activate fortuitously, it's advisable that you do ALL mode switches keeping the reset button pressed during the change.

In addition to these possibilities, the fact that Program mode configures a Von Neumann-style architecture can be exploited to run a debugger designed for this mode, such as [MO52](http://www.grifo.com/SOFT/uk_mo52.htm) or [PAULMON2](https://www.pjrc.com/tech/8051/paulmon2.html) (without the flash writing feature). Note however that this requires you to burn the software into the internal ROM, or if it is OTP, to keep several chips around with the different ROM codes you're using. A ZIF socket for the MCU is recommended if you're going to burn different ROMs often; the board is made with enough clearance for such kind of socket (except maybe for the bypass capacitor - try to solder it on the bottom if you have problems with it, or even omit it).

## Memory Map

There are many 8051 type microcontrollers which feature an internal programmable ROM, but most if not all of them allow executing code in external memory without deactivating said ROM. This external memory begins immediately after the ROM ends; since the ROM size varies between models, the memory map in Program mode is not fixed (it is fixed for a certain model, though).

To save gates, the A16 line of the RAM has the reverse polarity to the one that would probably result most intuitive. In program mode it's fixed to 1, and in run mode it's 1 for code memory and 0 for data memory.

The resulting memory map is as follows:

```
                    *Program mode*

        Code memory                  Data memory
 MCU addr          RAM addr   MCU addr          RAM addr
         +--------+                   +--------+
    FFFF |        | 1FFFF        FFFF |        | 1FFFF
         |        |                   |        |
         |        |                   |        |
         |        |                   |        |
         | shared |                   | shared |
         |  with  |                   |  with  |
         |  data  |                   |  code  |
         |  mem.  |                   |  mem.  |
         |        |                   |        |
         |        |                   |        |
         |        |                   |        |
    XXXX |        | 1XXXX        XXXX |        | 1XXXX
         +--------+                   +--------+
  XXXX-1 |        |  N/A       XXXX-1 |  data  | 1XXXX-1
         |internal|                   |  mem.  |
         |  ROM   |                   |  only  |
    0000 |        |  N/A         0000 |        | 10000
         +--------+                   +--------+

XXXX = internal ROM size
```

```
                       *Run mode*

        Code memory                  Data memory
 MCU addr          RAM addr   MCU addr          RAM addr
         +--------+                   +--------+
    FFFF |        | 1FFFF        FFFF |        | 0FFFF
         |        |                   |        |
         |        |                   |        |
         |        |                   |        |
         |        |                   |        |
         |        |                   |        |
         |        |                   |        |
         |        |                   |        |
         |        |                   |        |
         |        |                   |        |
         |        |                   |        |
         |        |                   |        |
         |        |                   |        |
         |        |                   |        |
         |        |                   |        |
    0000 |        | 10000        0000 |        | 00000
         +--------+                   +--------+
```

## Internals

The board is a pretty standard micro + latch + external memory configuration, plus the mode switch logic, so I will focus on the latter.

The lines to monitor and control for the mode switch are /EA, /PSEN and /RD, on the MCU side, and /OE and A16, on the RAM side. The mode switch is 1 in Program mode (via a pullup) and 0 in Run mode. This goes directly to the /EA input, so that the internal RAM is only active in Program mode. As the memory map shows, A16 is 0 when in Run mode and /PSEN is 1 (inactive), and 1 otherwise. /OE will be active when either /RD or /PSEN are active, since both code and data accesses read from the same RAM in all cases; it's A16 the one that decides which RAM section to read depending on the mode and /PSEN.

If you have a smaller RAM, say 64KB, it's possible to change A16 to A15 and have 32KB + 32KB instead of 64KB + 64KB. In this case, the lower 32KB of RAM of each section will be mirrored on the higher 32 KB. For a 32 KB RAM, A16 can be changed to A14 instead, resulting in 16KB + 16KB with four mirrors of each, etc. This will require you to modify the schematics and the PCB accordingly.

## The Board

The repository itself contains only the KiCad files - schematics, PCB and extra libraries used. I'm not sure of the purpose of the `-rescue.*` files, but I've included them anyway. In the [Releases](../../../../releases) page you can find PDF files for the schematics and the PCB, and a ZIP file containing the Gerber files.

### Bill of materials

Numbers in parentheses are footnotes.

```
C1 30pF (1)
C2 30pF (1)
C3 1µF (2)
C4 100µF
C5 100nF (3)
C6 100nF
C7 100nF
C8 100nF
D1 LED
J1 Pin header connector, 1x4 pin
J2 Pin header connector, 2x8 pin
R1 47K
R2 10K
R3 1K (4)
SW1 SPST-NO Push button
SW2 SPST-NO switch (5)
U1 AT89C52 (6)
U2 74HC573
U3 74HC00
U4 TC551001 (7)
Y1 12 MHz (8)
```

#### Footnotes

(1) C1 and C2 depend on the crystal characteristics and can sometimes be omitted. Intel recommends 30pF for a 12 MHz crystal, which is what is specified.

(2) This capacitor is specified as ceramic; if you don't have one of those, you can use an electrolytic one if you connect the + to the +5V.

(3) If installing a ZIF socket for the MCU, there may not be enough room for this bypass capacitor. In that case, you can install it on the other side or just omit it altogether.

(4) This depends on your LED; use a suitable resistor.

(5) There are several options for this switch, for example it can be SPDT with 0.1" or DIP-2.

(6) This is the micro I used in my case. Many choices are possible.

(7) Any voltage and pinout compatible RAM can be used; check also the speed of the RAM vs the MCU access timing.

(8) Many crystals are possible, but note that the rest of ICs need to support the chosen one, so you will need to check datasheets, like in (7). I installed a socket to allow me to switch crystals. Note also that the crystal frequency will determine the baud rate. If your RS232 interface or communications program supports arbitrary baud rates, you don't have to worry; just check which bauds are available for that frequency. If it only supports standard speeds, the following list will help you pick a crystal (if you use a crystal with double frequency to one of those, you can double the bauds). Remember that only Timer 1 comes as default; if your micro is the 8052 type, you can use Timer 2 as a baud rate generator, but you'll have to code the Timer 2 support into deb51 yourself.

  - 11.0592 MHz: Any standard baud rate up to 115,200 with timer 2; any between 300 and 57,600 with timer 1.
  - 12.0000 MHz: Max. 4,800 bauds with timer 1; max. 28,800 with timer 2.
  - 16.0000 MHz: Max. 38,400 bauds with timer 2; with Timer 1 you can get either 28,800 (with a 3.55% error - not recommended) or 2400 (for under 1% error - recommended).

For other crystals, check https://www.keil.com/products/c51/baudrate.asp for your baud options. Enter the crystal frequency and click "Calculate" to obtain a table including the obtainable baud rates for several timer values, and the percent of error with respect to standard baud rates.

Note that fast baud rates using timer 2 are untested; I use a crystal of 24 MHz and 125,000 bauds with timer 1. The Load command has a baud divisor limit; it supports Timer 1 settings of up to the maximum timer reload value of FFh, but the maximum speeds attainable with Timer 2 are higher, and there's a limit to what the load command can keep up with - I just don't know what that limit is.

## License

The license is CC BY-SA 4.0; check the file [LICENSE.txt](LICENSE.txt) for details, or check https://creativecommons.org/licenses/by-sa/4.0/ for a summary and the legal text in several languages.
