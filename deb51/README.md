# Deb51 - Loader and Memory Monitor for 8051

Deb51 is a program for 8051 that allows you to load binary files into external memory, as well as examine and alter the contents of said memory.

It was written as a companion for the [Dev8051](../Dev8051) minimalistic software development board, but of course it may find uses in other projects.

It doesn't make use of any 8052 features, so it can be used in any 8051-compatible MCU.

## Building

The assembly language dialect used is that of the sdas8051 assembler, which seems well maintained and generates linkable files.

The prerrequisites for building are:

- GNU make
- sdas8051, sdld and makebin - They are part of the SDCC (Small Device C Compiler) suite (makebin is only used for converting from Intel HEX to binary; you won't need it if you're not interested in the binary).

Before building, you may need to edit `conf.inc` to suit your needs; specifically the clock frequency, the communication speed in bauds, and the size of the internal EPROM/EEPROM/Flash etc. of the MCU you're using. Note that the acceptable baud rates depend on the frequency of the crystal used.

Included is a simple Makefile designed for GNU make. To build, just type `make`. This will generate some files with extension `.rel` which are relocatable (linkable) files, a file named `deb51.ihx` and a file named `deb51.bin`. If you're not interested in the binary, you can type `make deb51.ihx` instead of just `make`.

## Usage

The monitor has a series of commands designed to operate with external memory. The prompt is a dash (`-`). All commands that accept a number as the argument also admit an expression. Expressions are quite simple, the only accepted operators are addition, subtraction and signs, and they should have no spaces (otherwise it's not possible to distinguish an expression from an argument separator). Numbers are hexadecimal by default, with an optional `h` suffix; they can also be binary (with a `y` suffix) or decimal (with a `t` suffix). All numbers are truncated to 16 bits; negatives are represented as unsigned numbers in two's complement.

Some commands accept a range. A range consists of either a starting address and an ending address, or a starting address, the letter `L` and a length in bytes. Note that length 0 behaves actually as length 65536 (10000h), because otherwise it would not be possible to enter this number due to the 16 bit truncation.

Here's a list of commands and their functions:

- `?` (without arguments): shows a help text with a brief explanation of the commands.
- `? <number>`: Display the number in hex, decimal and ASCII.
- `C <range> <compare-to address>`: Compare the memory in the given range with a block of the same length starting at the given compare-to address. Matches are not displayed; only the differing bytes are shown. The output format for differences is `<origin address> <origin byte> <compare-to byte> <compare-to address>`.
- `D <optional range>`: Dump memory in human-friendly hexadecimal and ASCII. The range is optional; if specified, the end address or length is optional too. By default the start address is the one following the last dumped address (initially 0), and the length is 128 bytes.
- `DX <optional range>`: Dump memory in Intel HEX format. Same syntax for the range as in the `D` command.
- `E <address> <byte> [<byte>...]`: Enter a byte or series of bytes into the given address. Only the lower 8 bits of each byte are written.
- `F <range> <byte> [<byte>...]`: Fill a portion of memory with a byte or series of bytes. Only the lower 8 bits of each byte are written.
- `G=<address>`: Go (jump) to the given address.
- `I <sfr address>`: Read a special-function register (SFR) from the given address and display its value. It's intended to read from the I/O ports (P0, P1, P2 and P3, at addresses 80h, 90h, A0h and B0h respectively), but some MCUs have extra ports and there's no fixed standard for those; for example some Intel and Philips MCUs use C0h for P4 while the C8051F020 uses 88h, and there are even more inconsistencies with port P5, therefore a general SFR reading mechanism was provided instead of using fixed addresses.
- `L <optional offset>`: Load Intel HEX, optionally offset by the given value.
- `M <range> <address>`: Move a block of memory. Due to a technical constraint, the block to move must be 65535 bytes or less; accepting a length of 65536 bytes would require extra code and it was considered a very marginal use case not worth the effort. Using a length of 0 will only give an error.
- `O <sfr address> <value>`: Write to a special-function register. See comment on the `I` command for an explanation on the address. Only the lower 8 bits are written.
- `Q`: Jump past the internal MCU ROM (to the address configured in `conf.inc`).
- `S <range> <byte> [<byte>...]`: Search a range for the given sequence. All matching addresses are displayed.
- `V <optional range>`: Verify if a dump in Intel HEX format matches the memory contents.

Note that for performance, the `L` command stores bytes immediately rather than at the end of the line. This can have consequences; for example, if a checksum fails, the bytes will have been written despite the error. This is done to improve the maximum baud rate attainable during loads, since there is no flow control.

The `L` and `V` commands do not echo the input file; instead they display a dot per line, as a progress indicator.

The `C`, `F` and `M` commands rely on port P2 being bits 8-15 of the external address space. They may not work correctly on embedded RAM that is accessed via MOVX, if the micro has that.

## Debugger

This code does NOT contain a debugger: no disassembly, no breakpoints, no stepping; only memory and I/O operations. It's possible to incorporate one in future, though.

## License

The code is written by Pedro Gimeno Fortea, and offered under the GNU General Public License version 3. See the file COPYING for details.

From the license text I'd like to highlight this: you can use this code for your own personal use without any restrictions, but if you *distribute* compiled code that contains even a part of this work (including for example selling or giving away devices containing chips with code that uses this work), you must as well offer the full source code for exactly the code that you distribute, and additionally, the chip ROM should be replaceable by the person you give it to. Again, for full details please read the file COPYING carefully.
