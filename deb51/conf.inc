; Deb51 loader and memory debugger for 8051
; Copyright 2022 Pedro Gimeno Fortea
; Assembly language Dialect: sdas8051
;
;   This file is part of Deb51.
;
;   Deb51 is free software: you can redistribute it and/or modify it under the
;   terms of the GNU General Public License as published by the Free Software
;   Foundation, either version 3 of the License, or (at your option) any later
;   version.
;
;   Deb51 is distributed in the hope that it will be useful, but WITHOUT ANY
;   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;   details.
;
;   You should have received a copy of the GNU General Public License along
;   with Deb51. If not, see <https://www.gnu.org/licenses/>.
;
; This file contains user-configurable definitions.


; Defs
XTAL		.equ	24000000
BAUD		.equ	125000

ROMEND		.equ	0x2000
USROFS		.equ	ROMEND		; User program offset
