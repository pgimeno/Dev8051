; Deb51 loader and memory debugger for 8051
; Copyright 2022 Pedro Gimeno Fortea
; Assembly language Dialect: sdas8051
;
;   This file is part of Deb51.
;
;   Deb51 is free software: you can redistribute it and/or modify it under the
;   terms of the GNU General Public License as published by the Free Software
;   Foundation, either version 3 of the License, or (at your option) any later
;   version.
;
;   Deb51 is distributed in the hope that it will be useful, but WITHOUT ANY
;   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;   details.
;
;   You should have received a copy of the GNU General Public License along
;   with Deb51. If not, see <https://www.gnu.org/licenses/>.
;
; This is the main file, with the interrupt vectors, the initialization code
; and the command line input/process loop.

		.include "defs.inc"
		.include "conf.inc"

		; Exports
		.globl	RESET
		.globl	Input

		; Imports
		.globl	GetChar
		.globl	PutChar
		.globl	PutStrC
		.globl	PutNewLine
		.globl	Upper
		.globl	ParseCmdLine

; Comm management
M1T1BDBL	.equ	XTAL/192	; Mode 1 Timer 1 Base @ Double baud rate
M1T1BSNG	.equ	XTAL/384	; M1 T1 Base @ Single baud rate (unused)
M1T2B		.equ	XTAL/32		; Mode 1 Timer 2 Base (unused)
; We use Timer 1 with double baud rate, so use M1T1BDBL
BaudTimerVal	.equ	(M1T1BDBL+BAUD/2)/BAUD


		.area	VECTORS (CODE,ABS,CON)

		.org	0
		ajmp	RESET

		.org	0x0003
		; External Interrupt 0
		ljmp	0x0003+USROFS

		.org	0x000B
		; Timer 0 Interrupt
		ljmp	0x000B+USROFS

		.org	0x0013
		; External Interrupt 1
		ljmp	0x0013+USROFS

		.org	0x001B
		; Timer 1 Interrupt
		ljmp	0x001B+USROFS

		.org	0x0023
		; Serial Port Interrupt
		ljmp	0x0023+USROFS

		.org	0x002B
		; Timer 2 Interrupt (8052)
		; SIO1 Interrupt (I2C) (80C552)
		ljmp	0x002B+USROFS

		.org	0x0033
		; Timer 2 Capture 0 (80C552)
		ljmp	0x0033+USROFS

		.org	0x003B
		; Timer 2 Capture 1 (80C552)
		ljmp	0x003B+USROFS

		.org	0x0043
		; Timer 2 Capture 2 (80C552)
		ljmp	0x0043+USROFS

		.org	0x004B
		; Timer 2 Capture 3 (80C552)
		ljmp	0x004B+USROFS

		.org	0x0053
		; ADC completion (80C552)
		ljmp	0x0053+USROFS

		.org	0x005B
		; Timer 2 Compare 0 (80C552)
		ljmp	0x005B+USROFS

		.org	0x0063
		; Timer 2 Compare 1 (80C552)
		ljmp	0x0063+USROFS

		.org	0x006B
		; Timer 2 Compare 2 (80C552)
		ljmp	0x006B+USROFS

		.org	0x0073
		; Timer 2 Overflow (80C552)
		ljmp	0x0073+USROFS

		.org	0x007B
		; Unused interrupt service
		ljmp	0x007B+USROFS

		.org	ROMEND-3	; room fora a LJMP
		; Jump to RESET; assumes all other bytes are NOPs
		ljmp	0

		.area	DATA (CODE,REL,CON)

;PressRetMsg:	.asciz	"Press [RETURN]..."
Banner:		.ascii	"\r\nDeb51 v1.0 - (C) Copyright 2022 Pedro Gimeno Fortea\r\n"
		.asciz	"Type ? for help\r\n"

Prompt:		.asciz	"-"
DelSeq:		.db	8,' ',8,0

		.area	TEXT (CODE,REL,CON)

RESET:		clr	A
		mov	IE,A
		mov	IP,A
		mov	PSW,A
		mov	SP,#StartSP
		mov	R0,#255
ClrLoop:	mov	@R0,A
		djnz	R0,ClrLoop

		orl	PCON,#0x80	; Double baud rate

		mov	A,#256-BaudTimerVal
		mov	TH1,A		; Baud divisor (reload value)
		mov	TL1,A		; Baud divisor (preload value)

		mov	SCON,#0x52	; Serial mode 1, TI=1 (pretend that a
					;  char has finished sending)
		mov	TMOD,#0x20	; Timer mode: auto-reload 1
		mov	TCON,#0x40	; Start Timer 1 to generate baud

;		mov	DPTR,#PressRetMsg
;		acall	PutStrC
;WaitReturn:	acall	GetChar
;		cjne	A,#Enter,WaitReturn
		mov	DPTR,#Banner
		acall	PutStrC

; Show prompt and input a line
Input:		mov	DPTR,#Prompt
		acall	PutStrC
		mov	R0,#InpBuf
; Main input loop
InpLoop:	acall	GetChar
		cjne	A,#Enter,InpNoEnter
		sjmp	HandleEnter
InpNoEnter:	cjne	A,#DelChar1,InpNoDel1
		cjne	R0,#InpBuf,DelBuf	; don't delete if buffer empty
		sjmp	InpLoop		; buffer empty, ignore
InpNoDel1:	cjne	A,#DelChar2,InpNoDel2
		cjne	R0,#InpBuf,DelBuf
		sjmp	InpLoop
InpNoDel2:	cjne	A,#XOFF,InpNoXoff
InpLpXoff:	acall	GetChar
		cjne	A,#XON,InpLpXoff
		sjmp	InpLoop
InpNoXoff:	; All accepted control characters are handled at this point
		add	A,#256-32	; A < 32?
		jnc	InpLoop		; Ignore other control chars
		add	A,#32
		cjne	R0,#InpBuf+InpBufMaxLen-1,InpRoom	; leave 1 byte
		sjmp	InpLoop

DelBuf:		mov	DPTR,#DelSeq
		acall	PutStrC
		dec	R0
		sjmp	InpLoop

; There's room to store the char - check if it's valid hex and store it if so
InpRoom:	acall	PutChar
		mov	@R0,A
		inc	R0
		sjmp	InpLoop

HandleEnter:	acall	PutNewLine
		clr	A
		mov	@R0,A		; Add NUL terminator for easier parsing
		mov	A,R0
		mov	R0,#InpBuf	; Prepare pointer to start
		clr	C
		subb	A,R0
		jz	Input		; if command line empty
;		mov	InpLen,A	; store length of cmd line
		mov	R2,A		; length in R2

		acall	ParseCmdLine
		ajmp	Input
