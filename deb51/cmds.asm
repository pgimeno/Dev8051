; Deb51 loader and memory debugger for 8051
; Copyright 2022 Pedro Gimeno Fortea
; Assembly language Dialect: sdas8051
;
;   This file is part of Deb51.
;
;   Deb51 is free software: you can redistribute it and/or modify it under the
;   terms of the GNU General Public License as published by the Free Software
;   Foundation, either version 3 of the License, or (at your option) any later
;   version.
;
;   Deb51 is distributed in the hope that it will be useful, but WITHOUT ANY
;   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;   details.
;
;   You should have received a copy of the GNU General Public License along
;   with Deb51. If not, see <https://www.gnu.org/licenses/>.
;
; This file contains the command execution code for each command, as well as
; some command-specific auxiliary routines and the ReportError routine.

		.include "defs.inc"
		.include "conf.inc"

		; Exports
		.globl	ParseCmdLine
		.globl	ReportError

		; Imports
		.globl	Input
		.globl	PutStrC
		.globl	FindNonSpace
		.globl	MustBeEOL
		.globl	ParseExpr
		.globl	ParseRange
		.globl	ParseRangeDefL
		.globl	Upper
		.globl	PutChar
		.globl	PutHex
		.globl	PutDec
		.globl	PutPrintable
		.globl	PutNewLine
		.globl	GetChar
		.globl	DigitHex2Num

		.area	DATA (CODE,REL,CON)

		; IMPORTANT: .dw is big-endian in sdas8051
CmdList:	.dw	'?'*256, HelpAndPrint
		.dw	'D'*256, DumpHuman
		.dw	'D'*256+'X', DumpIHX
		.dw	'Q'*256, JumpUser
		.dw	'L'*256, LoadCmd
		.dw	'V'*256, VerifyCmd
		.dw	'E'*256, EnterCmd
		.dw	'G'*256, GoCmd
		.dw	'#'*256, retinstr1
		.dw	'I'*256, InputCmd
		.dw	'O'*256, OutputCmd
		.dw	'S'*256, SearchCmd
		.dw	'F'*256, FillCmd
		.dw	'M'*256, MoveCmd
		.dw	'C'*256, CompareCmd
		.db	0	; Terminator

HelpText:	.ascii	"? xxxx        Display expression\r\n"
		.ascii	"L [xxxx]      Load Intel HEX (optionally with an offset)\r\n"
		.ascii	"V [xxxx]      Verify Intel HEX previously loaded\r\n"
		.ascii	"G =xxxx       Go to xxxx\r\n"
		.ascii	"Q             Go to start of user program\r\n"
		.ascii	"D [xxxx [[L] yyyy]]         Dump memory (HEX + ASCII)\r\n"
		.ascii	"DX [xxxx [[L] yyyy]]        Dump memory in Intel HEX\r\n"
		.ascii	"E xxxx vv [vv ...]          Enter memory contents\r\n"
		.ascii	"F xxxx [L] yyyy vv [vv ...] Fill memory with byte(s)\r\n"
		.ascii	"S xxxx [L] yyyy vv [vv ...] Search memory\r\n"
		.ascii	"C xxxx [L] yyyy zzzz        Compare memory\r\n"
		.ascii	"M xxxx [L] yyyy zzzz        Move memory\r\n"
		.ascii	"I <sfr>                     Input from port\r\n"
		.ascii	"O <sfr> vv                  Output to port\r\n"
		.ascii	"# <anything>                Comment\r\n"
		.ascii	"Arguments are expressions; they can be:\r\n"
		.ascii	"   numbers, hexadecimal if H suffix (optional); decimal if T (Ten) suffix,\r\n"
		.ascii	"   binarY if Y suffix\r\n"
		.ascii	"   addition, subtraction or negative numbers (always 16 bits)\r\n"
		.ascii	"In ranges, L indicates that the next number is length; otherwise it's end addr\r\n"
		.db	0

ErrStr:		.ascii	"^ "
ErrCmdStr:	.asciz	"Error\007\r\n"

FmtNumPt1:	.asciz	"h  ("
FmtNumPt2:	.asciz	")  \042"
FmtNumPt3:	.asciz	"\042\r\n"
ihxEOFmarker:	.asciz	":00000001FF\r\n"

		.area	TEXT (CODE,REL,CON)

FindCmd:	push	ACC
		mov	DPTR,#CmdList
nextLoop:	clr	A
		movc	A,@A+DPTR	; First char in command
		jz	endOfList
		cjne	A,CmdChar1,findCmdNext	; Jump if not matched
		mov	A,#1		; Second char
		movc	A,@A+DPTR
		cjne	A,CmdChar2,findCmdNext
		mov	A,#2		; Addr High
		movc	A,@A+DPTR
		mov	R7,A		; Keep
		mov	A,#3
		movc	A,@A+DPTR	; Addr Low
		mov	DPL,A		; Set DPTR
		mov	DPH,R7
		clr	C
		sjmp	retFindCmd
findCmdNext:	inc	DPTR
		inc	DPTR
		inc	DPTR
		inc	DPTR
		sjmp	nextLoop
endOfList:	setb	C
retFindCmd:	pop	ACC
retinstr1:	ret


ParseCmdLine:	acall	FindNonSpace
		jz	retinstr1
		acall	Upper
		mov	CmdChar1,A
		inc	R0		; Eat char (might be uneaten later)
		mov	A,@R0
		jz	back1cmdChar	; if end of string, check 1 char cmd
		acall	Upper
		mov	CmdChar2,A
		acall	FindCmd		; See if the two chars match a command
		jnc	gotCmd		; If success, got it
back1cmdChar:	dec	R0		; Uneat char
		mov	CmdChar2,#0	; Clear 2nd char to see if it matches
		acall	FindCmd
		jnc	gotCmd		; If success, use 1 char cmd
		; Fall through to ReportError


ReportError:	mov	SP,#StartSP	; Restore SP on error
		mov	A,R0
		add	A,#256-InpBuf+1
		mov	R2,A
		mov	A,#' '
fillToColumn:	acall	PutChar
		djnz	R2,fillToColumn
		mov	DPTR,#ErrStr
		acall	PutStrC
		ajmp	Input

gotCmd:		inc	R0		; Eat last cmd char
		clr	A
		jmp	@A+DPTR		; Process command


; **** COMMANDS ****


; Q command

JumpUser:	mov	SP,#StartSP
		ljmp	USROFS


; ? command

HelpAndPrint:	acall	FindNonSpace
		jz	showHelp
		acall	ParseExpr
		acall	MustBeEOL

		mov	A,NumH
		acall	PutHex
		mov	A,NumL
		acall	PutHex
		mov	DPTR,#FmtNumPt1
		acall	PutStrC
		acall	PutDec
		mov	DPTR,#FmtNumPt2
		acall	PutStrC
		mov	A,NumH
		acall	PutPrintable
		mov	A,NumL
		acall	PutPrintable
		mov	DPTR,#FmtNumPt3
		ajmp	PutStrC

showHelp:	mov	DPTR,#HelpText
		ajmp	PutStrC


; D command

DumpHuman:	acall	FindNonSpace
		mov	R2,#128
		mov	R3,#0		; Default 128 bytes
		jz	dhDefaults
		acall	ParseRangeDefL
		acall	MustBeEOL
		mov	DataPtrL,DPL
		mov	DataPtrH,DPH
dhDefaults:
		mov	A,DataPtrL
		add	A,R2
		mov	R2,A
		mov	A,DataPtrH
		addc	A,R3
		mov	R3,A
dumpLine:
		mov	A,DataPtrH
		mov	DPH,A
		acall	PutHex
		mov	A,DataPtrL
		mov	DPL,A
		anl	A,#0xF0
		acall	PutHex
		mov	A,DataPtrL
		anl	A,#0x0F
		mov	R4,A
		rl	A		; x 2
		add	A,R4		; x 3
		inc	A		; x 3 + 1
		mov	R4,A

spacesAfterAdr:	mov	A,#' '
		acall	PutChar
		djnz	R4,spacesAfterAdr

prtHex:		mov	A,DPL
		anl	A,#0xF
		cjne	A,#8,dumpAddSpace
		mov	A,#'-'
		sjmp	dumpAddSep
dumpAddSpace:	mov	A,#' '
dumpAddSep:	acall	PutChar
		movx	A,@DPTR
		acall	PutHex
		inc	DPTR
		mov	A,R2
		cjne	A,DPL,moreHex
		mov	A,R3
		cjne	A,DPH,moreHex
		sjmp	epilogHex
moreHex:	mov	A,DPL
		anl	A,#0x0F
		jnz	prtHex

epilogHex:	clr	A
		clr	C
		subb	A,DPL
		anl	A,#0x0F
		mov	R4,A
		rl	A		; x 2
		add	A,R4		; x 3
		inc	A		; x 3 + 1
		mov	R4,A
spacesAfterHex:	mov	A,#' '		; Spaces until the ASCII area
		acall	PutChar
		djnz	R4,spacesAfterHex
		mov	DPH,DataPtrH
		mov	A,DataPtrL
		mov	DPL,A
		anl	A,#0x0F
		inc	A		; Extra space
		mov	R4,A
spacesB4asc:	mov	A,#' '
		acall	PutChar
		djnz	R4,spacesB4asc

dumpAsc:	movx	A,@DPTR
		inc	DPTR
		acall	PutPrintable
		mov	A,DPH
		mov	DataPtrH,A
		mov	A,DPL
		mov	DataPtrL,A
		mov	A,R2
		cjne	A,DPL,dumpNextAsc
		mov	A,R3
		cjne	A,DPH,dumpNextAsc
		ajmp	PutNewLine
dumpNextAsc:	mov	A,DPL
		anl	A,#0xF
		jnz	dumpAsc
		acall	PutNewLine
		ajmp	dumpLine


; DX command

DumpIHX:
		acall	FindNonSpace
		mov	R2,#128
		mov	R3,#0		; Default 128 bytes
		jz	dxDefaults	; Jump if no range present
		acall	ParseRangeDefL
		acall	MustBeEOL
		mov	DataPtrL,DPL
		mov	DataPtrH,DPH
dxDefaults:
		mov	DPL,DataPtrL
		mov	DPH,DataPtrH
;		mov	A,R2		;
;		jz	hexLine		; 8-bit loop trick
;		inc	R3		;
hexLine:	mov	A,#':'
		acall	PutChar
		mov	A,R3
		jnz	setMaxLength
		mov	A,R2
		jz	setMaxLength
		cjne	A,#0x20,(.)+3	; Max line length
		jc	gotLength
setMaxLength:	mov	A,#0x20		; Max line length
gotLength:	mov	R4,A
		mov	R5,A
		mov	R6,A		; cksum
		acall	PutHex
		mov	A,DPH
		acall	cksumPutHex
		mov	A,DPL
		acall	cksumPutHex
		clr	A		; Record type 00 = data
		acall	PutHex		; No need to checksum
dumpLineBytes:	movx	A,@DPTR
		inc	DPTR
		acall	cksumPutHex
		djnz	R4,dumpLineBytes
		clr	A		; Negate checksum
		clr	C
		subb	A,R6
		acall	PutHex		; Output cksum
		acall	PutNewLine
		clr	C
		mov	A,R2
		subb	A,R5		; Subtract line length from total
		mov	R2,A
		mov	A,R3
		subb	A,#0
		mov	R3,A
		orl	A,R2		; Is total length zero?
		jnz	hexLine		; Loop if not
		mov	DataPtrL,DPL	; Update data pointer with last dumped
		mov	DataPtrH,DPH
		mov	DPTR,#ihxEOFmarker
		ajmp	PutStrC

cksumPutHex:	xch	A,R6
		add	A,R6
		xch	A,R6
		ajmp	PutHex


; L command

LoadCmd:	clr	F0
		sjmp	LoadVerify
VerifyCmd:	setb	F0
LoadVerify:	clr	A
		mov	NumL,A
		mov	NumH,A
		acall	FindNonSpace
		jz	loadDefault
		acall	ParseExpr
loadDefault:
loadLine:	acall	GetChar
		cjne	A,#CancelChar,notInt
		ajmp	PutNewLine
notInt:		cjne	A,#':',loadDefault
		mov	R6,#0		; Init checksum
		acall	LdGetHexByte	; Length byte
		mov	R2,A
		acall	LdGetHexByte	; Address H
		mov	R3,A
		acall	LdGetHexByte	; Address L
		add	A,NumL		; Add offset
		mov	DPL,A
		mov	A,R3
		addc	A,NumH
		mov	DPH,A
		acall	LdGetHexByte	; Type of record
		jnz	recNotData
		mov	A,R2
		jz	skipData	; if length zero
dataLdVLoop:	acall	LdGetHexByte
		jnb	F0,isLoad
		mov	R7,A
		movx	A,@DPTR
		cjne	A,7,ErrLoad
		sjmp	cont
isLoad:		movx	@DPTR,A
cont:		inc	DPTR
		djnz	R2,dataLdVLoop
skipData:	acall	LdGetHexByte	; Checksum
csumChk:	cjne	R6,#0,ErrLoad
		mov	A,#'.'		; Progress indicator
		acall	PutChar
		sjmp	loadLine
recNotData:	cjne	A,#1,unkBlock	; No idea what this block is - skip
		acall	LdGetHexByte	; Checksum
		cjne	R6,#0,ErrLoad
		ajmp	PutNewLine	; End of data

unkBlock:	inc	R2		; for checksum
skipBlock:	acall	LdGetHexByte
		djnz	R2,skipBlock
		sjmp	csumChk

ErrCmd:
ErrLoad:	mov	DPTR,#ErrCmdStr
		acall	PutStrC
		sjmp	loadStop

LdGetHexByte:	acall	LdGetHexDigit
		swap	A
		mov	R7,A
		acall	LdGetHexDigit
		orl	A,R7
		xch	A,R6
		add	A,R6		; Checksum
		xch	A,R6
		ret

LdGetHexDigit:	acall	GetChar
		cjne	A,#CancelChar,chkHex
loadStop:	mov	SP,#StartSP
		acall	PutNewLine
		ajmp	Input
chkHex:		acall	DigitHex2Num
		jc	ErrLoad
		ret


; G command

errSyntax1:	ajmp	ReportError

GoCmd:		acall	FindNonSpace
		cjne	A,#'=',errSyntax1
		inc	R0
		acall	ParseExpr
		mov	DPL,NumL
		mov	DPH,NumH
		clr	A
		jmp	@A+DPTR

EnterCmd:	acall	ParseExpr
		mov	DPL,NumL
		mov	DPH,NumH
nextNum:	acall	ParseExpr
		mov	A,NumL
		movx	@DPTR,A
		inc	DPTR
		acall	FindNonSpace
		jnz	nextNum
		ret


; I command

InputCmd:	acall	ParseExpr
		acall	MustBeEOL
		mov	A,NumL
		acall	getPort
		acall	PutHex
		ajmp	PutNewLine


; O command

OutputCmd:	acall	ParseExpr
		mov	R2,NumL
		acall	ParseExpr
		acall	MustBeEOL
		mov	A,NumL
		xch	A,R2
		mov	DPTR,#PortWrBase
		ajmp	commonPort


; S command

SearchCmd:	acall	ParseRange
		mov	R1,#InpBuf	; We're writing InpBuf as we read it!
		; But we'll never overwrite what we're reading.
searchFindVals:	acall	ParseExpr
		clr	A
		clr	C
		subb	A,NumL		; Store in two's complement
		mov	@R1,A
		inc	R1
		acall	FindNonSpace
		jnz	searchFindVals
		; R1 = position after last byte to search for

		mov	A,R2
		jz	searchNoAdj
		inc	R3		; Make R3 usable in nested loop
		clr	F0		; Clear found flag
searchNoAdj:
		mov	R0,#InpBuf
couldMatch:	movx	A,@DPTR
		inc	DPTR
		add	A,@R0
		inc	R0
		jnz	searchNoMatch
		mov	A,R0
		cjne	A,1,couldMatch	; if R0 != R1, keep searching
		setb	F0		; Set match found flag
searchNoMatch:	mov	A,#InpBuf
		clr	C
		subb	A,R0		; must be negative
		add	A,DPL
		mov	DPL,A
		mov	A,DPH
		addc	A,#0xFF		; sign-extended negative value
		mov	DPH,A
		jnb	F0,searchNoPrint
		acall	PutHex
		mov	A,DPL
		acall	PutHex
		acall	PutNewLine
		clr	F0
searchNoPrint:	inc	DPTR
		djnz	R2,searchNoAdj
		djnz	R3,searchNoAdj
		ret


; F command

FillCmd:	acall	ParseRange
		mov	A,R2
		jz	fillNoAdj
		inc	R3		; prepare for double DJNZ
fillNoAdj:	mov	R1,#0

fillFindVals:	acall	ParseExpr
		mov	A,NumL
		movx	@DPTR,A
		inc	DPTR
		inc	R1		; Count # of bytes in pattern
		djnz	R2,fillNextVal	; Advance count
		djnz	R3,fillNextVal
		ret			; Finished if length consumed
fillNextVal:	acall	FindNonSpace
		jnz	fillFindVals

		mov	A,DPL
		clr	C
		subb	A,R1		; Go back by # of bytes in fill pattern
		mov	R0,A
		mov	A,DPH
		subb	A,#0
		mov	P2,A		; Origin pointer: P2:R0; dest: DPTR

copyMemUp:	movx	A,@R0
		movx	@DPTR,A
		inc	DPTR
		inc	R0
		cjne	R0,#0,copyUpSamePage
		inc	P2		; Increase page
copyUpSamePage:	djnz	R2,copyMemUp
		djnz	R3,copyMemUp
		mov	P2,#255		; it just feels right
		ret


; M command

MoveCmd:	acall	ParseRange
		acall	ParseExpr
		acall	MustBeEOL
		mov	A,R2
		orl	A,R3
		jz	localErrCmd1	; Length zero not supported
		; DPTR = source addr, R3:R2 = length, NumH:NumL = dest addr
		mov	A,NumL
		mov	R4,A
		clr	C
		subb	A,DPL
		mov	R6,A
		mov	A,NumH
		mov	R5,A		; R5:R4 = dest addr
		subb	A,DPH
		mov	R7,A		; R7:R6 = dest - src
		mov	A,R6		; Check carry of (dest - src) - len
		clr	C
		subb	A,R2
		mov	A,R7
		subb	A,R3		; Is dest - src < len?
		jc	moveDown	; If so, copy in descending direction
		mov	R0,DPL		; Set up registers for copyMemUp
		mov	P2,DPH		; P2:R0 = src
		mov	DPL,R4
		mov	DPH,R5		; DPTR = dest
		mov	A,R2
		jz	copyMemUp
		inc	R3		; Adjust R3:R2 for double DJNZ
		sjmp	copyMemUp

moveDown:	mov	A,DPL		; Add length to both src and dest
		add	A,R2
		mov	DPL,A
		mov	A,DPH
		addc	A,R3
		mov	DPH,A		; Src+Length to DPTR
		mov	A,R4
		add	A,R2
		mov	R0,A		; Dest+Length to P0:R2
		mov	A,R5
		addc	A,R3
		mov	P2,A
		mov	A,R2
		jz	moveDnLoop
		inc	R3		; Adjust for double DJNZ
moveDnLoop:	dec	R0		; Decrement dest
		cjne	R0,#255,noDecDestH
		dec	P2		; High byte needs decrementing too
noDecDestH:	mov	A,DPL		; For checking overflow on decrement
		dec	DPL		; Decrement src low byte
		jnz	noDecSrcH	; Skip if no overflow
		dec	DPH		; High byte needs decrementing too
noDecSrcH:	movx	A,@DPTR		; Copy from src to dest
		movx	@R0,A
		djnz	R2,moveDnLoop	; Repeat for as many bytes as specified
		djnz	R3,moveDnLoop
		mov	P2,#255		; it just feels right
		ret


localErrCmd1:	ajmp	ErrCmd


; C command

CompareCmd:	acall	ParseRange
		acall	ParseExpr
		acall	MustBeEOL
		; DPTR = first addr, R3:R2 = length, NumH:NumL = second addr
		mov	R4,NumH
		mov	P2,R4
		mov	R0,NumL
		mov	A,R2
		jz	cmpLoop
		inc	R3
cmpLoop:	movx	A,@DPTR
		mov	R6,A
		movx	A,@R0
		cjne	A,6,cmpDiffFound
cmpDiffPrinted:	inc	DPTR
		inc	R0
		cjne	R0,#0,cmpNoIncH
		inc	R4
		mov	P2,R4
cmpNoIncH:	djnz	R2,cmpLoop
		djnz	R3,cmpLoop
		mov	P2,#255		; It just feels right
		ret

cmpDiffFound:	mov	R5,A		; Difference found; keep value 2
		mov	A,DPH		; Print DPTR (1st address)
		acall	PutHex
		mov	A,DPL
		acall	PutHex
		acall	twoSpaces
		mov	A,R6		; Print value 1
		acall	PutHex
		acall	twoSpaces
		mov	A,R5		; Print value 2
		acall	PutHex
		acall	twoSpaces
		mov	A,R4		; Print R4:R0 (2nd address)
		acall	PutHex
		mov	A,R0
		acall	PutHex
		acall	PutNewLine
		sjmp	cmpDiffPrinted	; Continue

twoSpaces:	mov	A,#' '
		acall	PutChar
		mov	A,#' '
		ajmp	PutChar

getPort:	mov	DPTR,#PortRdBase
commonPort:	jnb	A.7,localErrCmd1
		clr	A.7
		mov	R7,A
		rl	A
		add	A,DPL
		mov	DPL,A
		clr	A
		addc	A,DPH
		mov	DPH,A
		mov	A,R7
		jmp	@A+DPTR

PortRdBase:
tmpcntr		.equ	0x80
		.rept	128
		mov	A,tmpcntr
		ret
tmpcntr		.equ	tmpcntr+1
		.endm
PortWrBase:
tmpcntr		.equ	0x80
		.rept	128
		mov	tmpcntr,R2
		ret
tmpcntr		.equ	tmpcntr+1
		.endm
