; Deb51 loader and memory debugger for 8051
; Copyright 2022 Pedro Gimeno Fortea
; Assembly language Dialect: sdas8051
;
;   This file is part of Deb51.
;
;   Deb51 is free software: you can redistribute it and/or modify it under the
;   terms of the GNU General Public License as published by the Free Software
;   Foundation, either version 3 of the License, or (at your option) any later
;   version.
;
;   Deb51 is distributed in the hope that it will be useful, but WITHOUT ANY
;   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;   details.
;
;   You should have received a copy of the GNU General Public License along
;   with Deb51. If not, see <https://www.gnu.org/licenses/>.
;
; This file contains the command and expression parser, and additional helper
; routines for general parsing.

		.area	TEXT (CODE,REL,CON)

		.include "defs.inc"

		; Exports
		.globl	FindNonSpace
		.globl	MustBeEOL
		.globl	ParseExpr
		.globl	ParseRange
		.globl	ParseRangeDefL

		; Imports
		.globl	ReportError
		.globl	DigitHex2Num
		.globl	Upper
		.globl	PutChar		; for debug

FindNonSpace:	mov	A,@R0
		cjne	A,#' ',retnonspace
		inc	R0
		sjmp	FindNonSpace
retnonspace:	ret

MustBeEOL:	acall	FindNonSpace
		jz	retnonspace
		ajmp	ReportError


ParseBin:	mov	A,@R0
		cjne	A,#'0',chk1
		sjmp	binDigitOK
chk1:		cjne	A,#'1',retCarry
binDigitOK:	add	A,#256-'0'
		mov	NumL,A
		mov	NumH,#0
grabNextBin:	inc	R0
		mov	A,@R0
		cjne	A,#'0',chk1b
		clr	C
nextDigitOK:	mov	A,NumL
		rlc	A
		mov	NumL,A
		mov	A,NumH
		rlc	A
		mov	NumH,A
		sjmp	grabNextBin
chk1b:		cjne	A,#'1',retNoCarry
		setb	C
		sjmp	nextDigitOK

retNoCarry:	clr	C
		ret
retCarry:	setb	C
		ret

ParseDec:	mov	A,@R0
		cjne	A,#'0',(.)+3
		jc	retCarry
		cjne	A,#'9'+1,(.)+3
		jnc	retCarry
		mov	NumL,#0
		mov	NumH,#0
decGotDigit:	inc	R0
		add	A,#256-'0'
		mov	R6,A
		mov	A,NumL
		mov	B,#10
		mul	AB
		mov	R4,A
		mov	R5,B
		mov	A,NumH
		mov	B,#10
		mul	AB
		add	A,R5
		mov	R5,A
		mov	A,R4
		add	A,R6
		mov	NumL,A
		mov	A,R5
		addc	A,#0
		mov	NumH,A
		mov	A,@R0
		cjne	A,#'0',(.)+3
		jc	retNoCarry
		cjne	A,#'9'+1,(.)+3
		jnc	retNoCarry
		sjmp	decGotDigit

ParseNumber:
		push	0
		acall	ParseBin
		jc	tryDec		; jmp if no valid binary digits
		acall	Upper
		cjne	A,#'Y',tryDec
		; Success - it was a binary number
		dec	SP		; drop R0 from the stack
		inc	R0		; eat Y
		ret

tryDec:		pop	0
		push	0
		acall	ParseDec
		jc	tryHex		; jmp if no valid decimal digits
		acall	Upper
		cjne	A,#'T',tryHex
		; Success - it was a decimal number
		dec	SP		; drop R0 from the stack
		inc	R0
		ret

tryHex:		pop	0

		; simple hex number processor
ParseHex:	mov	A,@R0
		acall	DigitHex2Num
		jnc	storeDigit
		ajmp	ReportError	; we do expect 1 digit at least
storeDigit:	inc	R0		; digit is valid and processed, eat it
		mov	NumL,A		; store first digit
		mov	NumH,#0
moreDigits:	mov	A,@R0
		acall	DigitHex2Num
		jc	eatHandRet
		inc	R0		; eat digit
		mov	R7,A		; say it's 5 for the sake of example
		mov	A,NumL		; 34
		swap	A		; 43
		anl	A,#0xF0		; 40
		orl	A,R7		; 45
		xch	A,NumL		; 34
		swap	A		; 43
		mov	R7,A		; 43
		mov	A,NumH		; 12
		swap	A		; 21
		xrl	A,R7
		anl	A,#0xF0
		xrl	A,R7		; 23
		mov	NumH,A		; 2345
		sjmp	moreDigits

eatHandRet:	mov	A,@R0
		acall	Upper
		cjne	A,#'H',retNow
		inc	R0		; Eat the H
retNow:		ret


ParseTerm:
		clr	F0
moreSigns:	mov	A,@R0
		cjne	A,#'-',notNegated
		inc	R0
		cpl	F0
		sjmp	moreSigns
notNegated:	cjne	A,#'+',ParseTerm2
		inc	R0
		sjmp	moreSigns
ParseTerm2:	acall	ParseNumber
		jnb	F0,retTerm	; Jump if positive
		clr	A		; Change sign
		clr	C
		subb	A,NumL
		mov	NumL,A
		clr	A
		subb	A,NumH
		mov	NumH,A
retTerm:	ret


ParseExpr:	acall	FindNonSpace
		acall	ParseTerm
moreTerms:	mov	OpL,NumL
		mov	OpH,NumH
		mov	A,@R0
		cjne	A,#'+',notAddition
		inc	R0
		mov	A,@R0

		acall	ParseTerm
		mov	A,NumL
		add	A,OpL
		mov	NumL,A
		mov	A,NumH
		addc	A,OpH
		mov	NumH,A
		sjmp	moreTerms

notAddition:	cjne	A,#'-',endExpr
		inc	R0
		mov	A,@R0
		acall	ParseTerm
		mov	A,OpL
		clr	C
		subb	A,NumL
		mov	NumL,A
		mov	A,OpH
		subb	A,NumH
		mov	NumH,A
		sjmp	moreTerms

endExpr:	ret


; Returns DPTR = start address, R3:R2 = length
ParseRange:	acall	ParseExpr
		mov	DPL,NumL
		mov	DPH,NumH
		acall	FindNonSpace
parseFullRange:	acall	Upper
		cjne	A,#'L',rangeEndAddr
		inc	R0		; Eat the L
		acall	ParseExpr
		mov	R2,NumL
		mov	R3,NumH
		ret

rangeEndAddr:	acall	ParseExpr
		mov	A,NumL
		add	A,#1
		mov	R2,A
		mov	A,NumH
		addc	A,#0
		xch	A,R2
		clr	C
		subb	A,DPL
		xch	A,R2
		subb	A,DPH
		mov	R3,A
		ret

; Parse a range; length or end address is optional
ParseRangeDefL:	acall	ParseExpr
		mov	DPL,NumL
		mov	DPH,NumH
		acall	FindNonSpace
		jnz	parseFullRange
		ret
