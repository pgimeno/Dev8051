; Deb51 loader and memory debugger for 8051
; Copyright 2022 Pedro Gimeno Fortea
; Assembly language Dialect: sdas8051
;
;   This file is part of Deb51.
;
;   Deb51 is free software: you can redistribute it and/or modify it under the
;   terms of the GNU General Public License as published by the Free Software
;   Foundation, either version 3 of the License, or (at your option) any later
;   version.
;
;   Deb51 is distributed in the hope that it will be useful, but WITHOUT ANY
;   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;   details.
;
;   You should have received a copy of the GNU General Public License along
;   with Deb51. If not, see <https://www.gnu.org/licenses/>.
;
; This file contains miscellaneous routines for general use.

		.include "defs.inc"

		.area	TEXT (CODE,REL,CON)

		; Exports
		.globl	PutHex
		.globl	PutChar
		.globl	PutPrintable
		.globl	PutNewLine
		.globl	HexDigits
		.globl	PutStrC
		.globl	GetChar
		.globl	Upper
		.globl	DigitHex2Num
		.globl	PutDec

		; Imports
		.globl	Input

PutHex:		push	ACC
		swap	A
		acall	PutHexDigit
		pop	ACC
PutHexDigit:	anl	A,#0xF
		add	A,#HexDigits-PutHexDig1
		movc	A,@A+PC
PutHexDig1:
		; Fall through to PutChar

PutChar:	jb	RI,PutChar1	; If something received while waiting
		jnb	TI,PutChar	; Keep waiting until transfer finishes
		mov	SBUF,A
		clr	TI
		ret
PutChar1:	mov	R7,SBUF		; Check byte received
		clr	RI
		cjne	R7,#CancelChar,PutChar2	; If not CancelChar, check XOFF
;		setb	AbortPut	; Set AbortPut flag
		mov	SP,#StartSP
		acall	PutNewLine
		ajmp	Input

PutChar2:	cjne	R7,#XOFF,PutChar; If not XOFF, ignore
WaitXON:	jnb	RI,WaitXON
		mov	R7,SBUF
		clr	RI
		cjne	R7,#XON,WaitXON	; Pause send until XON recv'd
		sjmp	PutChar

		; Needs to be within ~240 bytes of movc A,@A+PC in PutHex
HexDigits:	.ascii	"0123456789ABCDEF"

PutPrintable:	cjne	A,#32,(.)+3
		jc	notPrintable
		cjne	A,#127,(.)+3
		jc	isPrintable
notPrintable:	mov	A,#'.'
isPrintable:	ajmp	PutChar

PutNewLine:	mov	A,#13
		acall	PutChar
		mov	A,#10
		ajmp	PutChar


; Output a string in *code* memory (DPTR = string)
PutStrC:	push	ACC
PutStr1:	;jb	AbortPut,EndStr
		clr	A
		movc	A,@A+DPTR
		inc	DPTR
		jz	EndStr		; Jump if NUL terminator found
		acall	PutChar
		sjmp	PutStr1
EndStr:		;clr	AbortPut
		pop	ACC
		ret

GetChar:	jnb	RI,GetChar
		mov	A,SBUF
		clr	RI
		ret

DigitHex2Num:	add	A,#256-'0'	; Subtract ASC('0')
		jnc	ErrorHex	; Jump if < ASC('0')
		add	A,#256-10	; 0 to 9?
		jnc	ZeroToNine	; Jump if so
		clr	ACC.5		; Ignore case
		add	A,#256-7	; Less than 'A'?
		jnc	ErrorHex	; Jump if so
		add	A,#256-6	; Greater than 'F'?
		jc	ErrorHex	; Jump if so
		add	A,#16		; Map 'A'..'F' -> 10..15
;		setb	IsLetter
		clr	C
		ret
ZeroToNine:	add	A,#10		; Map '0'..'9' -> 0..9
;		clr	IsLetter
		clr	C		; No error
		ret
ErrorHex:	; clr	IsLetter
		setb	C		; Set carry to signify error
		ret

Upper:		add	A,#256-'a'
		jnc	urestore_ret
		add	A,#256-('z'+1-'a')
		jc	urestore2_ret
		add	A,#'Z'+1
		ret
urestore2_ret:	add	A,#'z'+1
		ret
urestore_ret:	add	A,#'a'
		ret

PutDec:		; Schoolbook division, nibble by nibble; four divisions are
		; performed in order to get all five digits.

		; First division by 10 to get the fifth digit.
		; Quotient goes to R3:R4.
		mov	A,NumH
		swap	A
		anl	A,#0x0F		; First nibble from dividend
		mov	R7,#10		; Divisor is always 10
		mov	B,R7
		div	AB		; Divide 1st nibble by 10
		mov	R3,A		; Keep quotient for later
		mov	A,B		; Calculate remainder*10h + next digit
		swap	A
		xrl	A,NumH
		anl	A,#0xF0		; Leave high nibble intact
		xrl	A,NumH
		mov	B,R7
		div	AB		; Divide remainder*10h + second nibble
		swap	A
		orl	A,R3
		swap	A
		mov	R3,A		; Complete R3 = high byte of quotient
		mov	A,NumL		; Prepare next remainder*10h+digit in A
		anl	A,#0xF0
		orl	A,B
		swap	A
		mov	B,R7
		div	AB		; Divide remainder*10h + third nibble
		mov	R4,A		; Second byte of quotient
		mov	A,B
		swap	A		; Prepare last remainder*10h+digit
		xrl	A,NumL
		anl	A,#0xF0		; Leave high nibble intact
		xrl	A,NumL
		mov	B,R7
		div	AB		; Divide remainder*10h + last nibble
		swap	A		; Complete second byte of quotient
		orl	A,R4
		swap	A
		mov	R4,A		; This completes the first division
		mov	R2,B		; Store the fifth digit in R2

		; Second division - the first nibble is always < 10 so we can
		; skip that partial division (maximum dividend is 1999h)
		mov	A,R3		; First two nibbles
		mov	B,R7
		div	AB		; Divide first byte as a whole
		mov	R3,A		; Keep 1st byte of quotient for later
		mov	A,R4		; Next nibble + remainder * 10h to A
		anl	A,#0xF0
		orl	A,B
		swap	A
		mov	B,R7
		div	AB		; Divide remainder*10h + next nibble
		swap	A		; Move quotient to high nibble
		xch	A,R4		; Store into R4, retrieve last nibble
		anl	A,#0x0F
		swap	A
		orl	A,B		; Combine with remainder
		swap	A
		mov	B,R7
		div	AB		; Divide remainder*10h + last nibble
		orl	A,R4
		mov	R4,A		; This completes the second division
		mov	R5,B		; Store fourth digit in R5

		; Third division - the first nibble is zero and the second
		; one is always < 10, so we combine the last nibble of R3
		; with the first of R4. The last nibble of R3 can't be greater
		; than 9 (maximum dividend is 28Fh) so no need to divide that
		; one either.
		xrl	A,R3		; actually A = R4 right now
		anl	A,#0xF0		; leave first nibble of A unchanged
		xrl	A,R3		; but grab last nibble from R3
		swap	A		; put them in the right order
		mov	B,R7
		div	AB		; divide first two significant nibbles
		mov	R3,A		; keep quotient for later
		mov	A,B		; prepare remainder*10h + last nibble
		swap	A
		xrl	A,R4
		anl	A,#0xF0		; Leave high nibble of A intact
		xrl	A,R4		; but grab low nibble from R4
		mov	B,R7
		div	AB		; This completes the third division
		mov	R4,B		; Store third digit into R4

		; Fourth and last division - in R3 we have the first nibble
		; and in A the last nibble of the quotient, which is a number
		; less than 100 (actually less than 65) so a single division
		; by 10 suffices to get the leading digit in the quotient and
		; the next one in the remainder.
		swap	A
		orl	A,R3
		swap	A
		mov	B,R7
		div	AB		; B=fourth digit, A=fifth digit
		; The divisions are now complete; we have:
		; A = first digit; B = second digit; R4 = third digit;
		; R5 = fourth digit; R2 = fifth digit. Now remove leading
		; zeros and convert to ASCII.
		jnz	PrtAll1		; Start on 1st digit if not zero
		mov	A,B
		jnz	PrtAll2		; Start on 2nd digit if not zero
		mov	A,R4
		jnz	PrtAll3		; Start on 3rd digit if not zero
		mov	A,R5
		jnz	PrtAll4		; Start on 4th digit if not zero
		sjmp	PrtAll5		; Start on 5th digit

PrtAll1:	add	A,#'0'
		acall	PutChar
		mov	A,B
PrtAll2:	add	A,#'0'
		acall	PutChar
		mov	A,R4
PrtAll3:	add	A,#'0'
		acall	PutChar
		mov	A,R5
PrtAll4:	add	A,#'0'
		acall	PutChar
PrtAll5:	mov	A,R2
		add	A,#'0'
		ajmp	PutChar
