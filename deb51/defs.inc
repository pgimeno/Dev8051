; Deb51 loader and memory debugger for 8051
; Copyright 2022 Pedro Gimeno Fortea
; Assembly language Dialect: sdas8051
;
;   This file is part of Deb51.
;
;   Deb51 is free software: you can redistribute it and/or modify it under the
;   terms of the GNU General Public License as published by the Free Software
;   Foundation, either version 3 of the License, or (at your option) any later
;   version.
;
;   Deb51 is distributed in the hope that it will be useful, but WITHOUT ANY
;   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;   details.
;
;   You should have received a copy of the GNU General Public License along
;   with Deb51. If not, see <https://www.gnu.org/licenses/>.
;
; This file contains general data definitions.

CancelChar	.equ	3		; Ctrl+C (ASCII <ETX>)
DelChar1	.equ	8		; ASCII <BS>
DelChar2	.equ	127		; ASCII <DEL>
Enter		.equ	13		; ASCII <CR>
XOFF		.equ	19		; ASCII <DC3>
XON		.equ	17		; ASCII <DC1>

; Variables in bit area
; Using variables high in the bit area gives us a bit more stack.
OpL		.equ	0x2A
OpH		.equ	0x2B
NumL		.equ	0x2C
NumH		.equ	0x2D
DataPtrL	.equ	0x2E
DataPtrH	.equ	0x2F
CmdChar1	.equ	0x2C		; not used at the same time as
CmdChar2	.equ	0x2D		; NumL/H

; Actual bit variables
;Flags		.equ	0x2F
;AbortPut	.equ	(Flags-0x20)*8+0; Bit 0 of Flags
; IsLetter	.equ	(Flags-0x20)*8+1; Bit 1 of Flags

; Variables in scratch area
;InpLen		.equ	0x30
;InpBuf		.equ	0x31
InpBuf		.equ	0x30
InpBufMaxLen	.equ	0x80 - InpBuf	; It's at the limit. With 8052 MCUs
					; it can be expanded further.

; Stack origin should be past the above vars
StartSP		.equ	8-1		; we only use RB0, we don't use RB1-RB3
					; 8051 stack is Full Ascending
