# Software development board and loader for 8051

Welcome to the repository for the Software Development Board for 8051, *[Dev8051](Dev8051),* with accompanying loader/monitor/memory debugger, *[deb51](deb51)*.

The 8051 has been a popular MCU and, as of this writing, is still in production by some brands. The main problem it has is that with possibly a few exceptions, it does not count with an in-circuit programming feature. Typically, replacing the program implies taking out a chip (a ROM or the MCU itself), placing it in the programmer (erasing it first if it's the UV type), reprogramming it and then placing it back into the board.

That process is cumbersome and it reduces the life of the ROM, plus we risk spoiling the socket or the chip pins. That's why this board was created: to allow for an easier way of testing software changes without taking any chip out of its socket.

At the same time, it provides two typical configurations: shared code/data RAM, and separate. The shared code/data mode requires new code to be loaded above the internal ROM space; this is the same configuration used by some monitors like MO52 and PAULMON2. In separate code/data mode, 64KB are dedicated to code and 64KB to data. It's the mode originally intended for running programs; the other mode was intended for loading them, and the shared mode was an unplanned extra bonus.

For more details on the hardware, click on the *[Dev8051](Dev8051)* link - it has its own README file. Similarly, for the software, click on the *[deb51](deb51)* link.

The license for the hardware is Creative Commons Attribution Share-Alike. The license for the software is the GNU General Public License version 3 or above.

The code and board were initially created by Pedro Gimeno Fortea. This is my first ever board and it was routed by hand; I know the separation between chips may be a bit excessive, but if you find other problems I'd be happy to learn about them.
